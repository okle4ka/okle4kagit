const PRICE = '$120';

function extractCurrencyValue(source) {
    if(source.length && typeof source === "string") {
        let newValue = source.replace('$', '');
        return Number(newValue);
    } else {
        return null;
    }
}

console.log(extractCurrencyValue(PRICE)); // 120
console.log(typeof extractCurrencyValue(PRICE)); // number
console.log(extractCurrencyValue({})); // null
