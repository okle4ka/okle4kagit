function truncate(string, maxLength) {
    if(string.length && typeof string === "string") {
        if(typeof maxLength === "number" && string.length > maxLength) {
            let extraCharacters = string.slice(maxLength - 3);
            return string.replace(extraCharacters, "...")
        } else {
            return string;
        }
    }
}

console.log(truncate('Вот, что мне хотелось бы сказать на эту тему:', 21)); // 'Вот, что мне хотел...'
