function upperCaseFirst(str) {
    if(str.length && typeof str === "string") {

        // first solution

        let firstChar = str.charAt(0);
        let withoutFirstChar = str.slice(1);
        const result = firstChar.toUpperCase() + withoutFirstChar;
        return result;

        // second solution

/*        let arrayFromString = Array.from(str);
        arrayFromString[0] = arrayFromString[0].toUpperCase();
        const result = (arrayFromString.toString()).replace(/,/g, "");
        return result;*/
    }
}

console.log(upperCaseFirst('pitter')); // Pitter
console.log(upperCaseFirst('')); //
