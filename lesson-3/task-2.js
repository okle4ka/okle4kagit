function checkSpam(source, spam) {
    let indexOfSubString = source.toLowerCase().indexOf((spam).toLowerCase());
    let res;
    if(indexOfSubString > 0) {
        res = true;
    } else {
        res = false;
    }
    const result = res;
    return result;
}

console.log(checkSpam('pitterXXX@gmail.com', 'xxx')); // true
console.log(checkSpam('pitterxxx@gmail.com', 'XXX')); // true
console.log(checkSpam('pitterxxx@gmail.com', 'sss')); // false
