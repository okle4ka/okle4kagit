const person = {};

Object.defineProperties(person, {
    salary: {
        get: function() {
            if(typeof this.rate === "number" && typeof this.hours === "number") {
                return "Зарплата за проект составляет" + ' ' + this.rate * this.hours + '$';
            } else {
                return "Bad data!";
            }
        }
    },
    rate: {
        value: 10,
        configurable: false
    },
    hours: {
        value: 200,
        configurable: false
    }
});

console.log(person.salary); // `Зарплата за проект составляет ххх$`
